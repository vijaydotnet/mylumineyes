
package com.mylumineyes.EyeDetection.camera;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.face.Landmark;
import com.google.android.gms.vision.face.LargestFaceFocusingProcessor;
import com.mylumineyes.Activity.CaptureRightEyeActivity;
import com.mylumineyes.Activity.EyesImagesActivity;
import com.mylumineyes.util.CommonUtills;
import com.mylumineyes.Fragments.Capture_RightEye;
import com.mylumineyes.Fragments.EyesImages;
import com.mylumineyes.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.hardware.Camera.Parameters.FLASH_MODE_AUTO;
import static android.hardware.Camera.Parameters.FLASH_MODE_OFF;
import static android.hardware.Camera.Parameters.FLASH_MODE_ON;
import static android.hardware.Camera.Parameters.FLASH_MODE_TORCH;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public final class GooglyEyesActivity extends AppCompatActivity implements Camera.AutoFocusCallback {
    private static final String TAG = "GooglyEyes";

    private static final int RC_HANDLE_GMS = 9001;
    //permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    public CameraSource mCameraSource = null;
    public CameraSourcePreview mPreview;
    public GraphicOverlay mGraphicOverlay;
    public static float eyeRadius;
    private boolean mIsFrontFacing = true;
    private static final int CAMERA_REQUEST = 1888;
    private static final int PERMISSION_REQUEST_CODE = 1000;

    private CameraManager mCameraManager;
    private String mCameraId;
    public static Bitmap bitmap = null,left_bitmap = null;
    public static String RightEyePath;
    public static String LeftEyePath;
    public String tp="";

    public  static android.hardware.Camera camera = null;

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        //Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.eyedectoractivity);

        boolean cmFlash = this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        Log.d("nn", String.valueOf(cmFlash));

        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay) findViewById(R.id.faceOverlay);

        tp = getIntent().getStringExtra("tp");

        if (tp != null) {
            try {
                camera = android.hardware.Camera.open();
                Log.d("kl", String.valueOf(camera));
                try {
                    Camera.Parameters parameters = camera.getParameters();
                    parameters.setFlashMode(getFlashOnParameter());
                    int maxZoom = parameters.getMaxZoom();
                    int zoom = 10;
                    if (parameters.isZoomSupported()) {
                        if (zoom >= 0 && zoom < maxZoom) {
                            parameters.setZoom(zoom);
                        } else {
                            //zoom parameter is incorrect
                        }
                    }
                    camera.setParameters(parameters);
                    camera.setPreviewTexture(new SurfaceTexture(0));
                    camera.startPreview();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                camera.takePicture(null, null, mPicture);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            if (savedInstanceState != null) {
                mIsFrontFacing = savedInstanceState.getBoolean("IsFrontFacing");
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                try {
                    mCameraId = mCameraManager.getCameraIdList()[0];
                } catch (@SuppressLint("NewApi") CameraAccessException e) {
                    e.printStackTrace();
                }
            }

            //Check for the camera permission before accessing the camera.  If the
            //permission is not granted yet, request permission.
            int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            if (rc == PackageManager.PERMISSION_GRANTED) {
                createCameraSource();
                startCameraSource();
            } else {
                requestCameraPermission();
            }

            int wext = ActivityCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE);
            if (wext == PackageManager.PERMISSION_GRANTED) {

            } else {
                requestCameraPermission();
            }

            if (ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            } else {

            }

        }
    }

    private String getFlashOnParameter() {
        List<String> flashModes = camera.getParameters().getSupportedFlashModes();
        if (flashModes.contains(FLASH_MODE_TORCH)) {
            return FLASH_MODE_TORCH;
        } else if (flashModes.contains(FLASH_MODE_ON)) {
            return FLASH_MODE_ON;
        } else if (flashModes.contains(FLASH_MODE_AUTO)) {
            return FLASH_MODE_AUTO;
        }
        throw new RuntimeException();
    }

    private void requestCameraPermission() {

        final String[] permissions = new String[]{Manifest.permission.CAMERA, WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPreview != null){
            mPreview.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source");
            //we have permission, so create the camerasource
            createCameraSource();
            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length + " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));
            if(requestCode == PERMISSION_REQUEST_CODE){
                int grantResultsLength = grantResults.length;
                if(grantResultsLength > 0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(getApplicationContext(), "You grant write external storage permission. Please click original button again to continue.", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(), "You denied write external storage permission.", Toast.LENGTH_LONG).show();
                }
            }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean("IsFrontFacing", mIsFrontFacing);
    }

    @NonNull
    private FaceDetector createFaceDetector(Context context) {
        FaceDetector detector = new FaceDetector.Builder(context)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .setTrackingEnabled(true)
                .setMode(FaceDetector.FAST_MODE)
                .setProminentFaceOnly(mIsFrontFacing)
                .setMinFaceSize(mIsFrontFacing ? 0.50f : 0.30f)
                .build();

        Detector.Processor<Face> processor;
        if (mIsFrontFacing) {
            Tracker<Face> tracker = new GooglyFaceTracker(mGraphicOverlay,GooglyEyesActivity.this);
            processor = new LargestFaceFocusingProcessor.Builder(detector, tracker).build();
        } else {
            MultiProcessor.Factory<Face> factory = new MultiProcessor.Factory<Face>() {
                @Override
                public Tracker<Face> create(Face face) {
                    return new GooglyFaceTracker(mGraphicOverlay,GooglyEyesActivity.this);
                }
            };
            processor = new MultiProcessor.Builder<>(factory).build();
        }

        detector.setProcessor(processor);

        if (!detector.isOperational()) {
            IntentFilter lowStorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowStorageFilter) != null;

        }
        return detector;
    }

    private void createCameraSource() {
        Context context = getApplicationContext();
        FaceDetector detector = createFaceDetector(context);

        int facing = CameraSource.CAMERA_FACING_BACK;
        if (!mIsFrontFacing) {
            facing = CameraSource.CAMERA_FACING_BACK;
        }
        mCameraSource = new CameraSource.Builder(context, detector)
                .setFacing(facing)
                .setRequestedPreviewSize(20, 40)
                .setRequestedFps(10.0f)
                .setAutoFocusEnabled(true)
                .build();
    }

    Camera.PictureCallback mPicture = new Camera.PictureCallback(){
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            BitmapFactory.Options opt = new BitmapFactory.Options();
            Log.d("bb",""+bitmap);
            opt.inMutable = true;
            if (bitmap == null && CommonUtills.EyeType.equalsIgnoreCase("R")){
                bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, opt);
                camera.stopPreview();
                camera.release();
                Intent intent = new Intent(GooglyEyesActivity.this, CaptureRightEyeActivity.class);
                startActivity(intent);
                finish();
            }else if (left_bitmap == null && CommonUtills.EyeType.equalsIgnoreCase("L")){
                camera.stopPreview();
                camera.release();
                left_bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, opt);
                Intent intent = new Intent(GooglyEyesActivity.this, EyesImagesActivity.class);
                startActivity(intent);
                finish();
            }

        }
    };

    private void startCameraSource() {
        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg = GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {

    }

    class GooglyFaceTracker extends Tracker<Face> {
        private static final float EYE_CLOSED_THRESHOLD = 0.4f;

        private GraphicOverlay mOverlay;
        private GooglyEyesGraphic mEyesGraphic;
        private Map<Integer, PointF> mPreviousProportions = new HashMap<>();
        Activity googlyEyesActivity1;

        private boolean mPreviousIsLeftOpen = true;
        private boolean mPreviousIsRightOpen = true;

        public GooglyFaceTracker(GraphicOverlay mGraphicOverlay, GooglyEyesActivity googlyEyesActivity) {
            mOverlay = mGraphicOverlay;
            googlyEyesActivity1=googlyEyesActivity;
        }

        @Override
        public void onNewItem(int id, Face face) {
            mEyesGraphic = new GooglyEyesGraphic(mOverlay,googlyEyesActivity1);
        }

        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mEyesGraphic);

            updatePreviousProportions(face);

            PointF leftPosition = getLandmarkPosition(face, Landmark.LEFT_EYE);
            PointF rightPosition = getLandmarkPosition(face, Landmark.RIGHT_EYE);

            float leftOpenScore = face.getIsLeftEyeOpenProbability();
            boolean isLeftOpen;
            if (leftOpenScore == Face.UNCOMPUTED_PROBABILITY) {
                isLeftOpen = mPreviousIsLeftOpen;
            } else {
                isLeftOpen = (leftOpenScore > EYE_CLOSED_THRESHOLD);
                mPreviousIsLeftOpen = isLeftOpen;
            }

            float rightOpenScore = face.getIsRightEyeOpenProbability();
            boolean isRightOpen;
            if (rightOpenScore == Face.UNCOMPUTED_PROBABILITY) {
                isRightOpen = mPreviousIsRightOpen;
            } else {
                isRightOpen = (rightOpenScore > EYE_CLOSED_THRESHOLD);
                mPreviousIsRightOpen = isRightOpen;
            }

            mEyesGraphic.updateEyes(leftPosition, isLeftOpen, rightPosition, isRightOpen);
        }

        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mEyesGraphic);
        }

        @Override
        public void onDone() {
            mOverlay.remove(mEyesGraphic);
        }

        private void updatePreviousProportions(Face face) {
            for (Landmark landmark : face.getLandmarks()) {
                PointF position = landmark.getPosition();
                float xProp = (position.x - face.getPosition().x) / face.getWidth();
                float yProp = (position.y - face.getPosition().y) / face.getHeight();
                mPreviousProportions.put(landmark.getType(), new PointF(xProp, yProp));
            }
        }

        private PointF getLandmarkPosition(Face face, int landmarkId) {
            for (Landmark landmark : face.getLandmarks()) {
                if (landmark.getType() == landmarkId) {
                    return landmark.getPosition();
                }
            }

            PointF prop = mPreviousProportions.get(landmarkId);
            if (prop == null) {
                return null;
            }

            float x = face.getPosition().x + (prop.x * face.getWidth());
            float y = face.getPosition().y + (prop.y * face.getHeight());
            return new PointF(x, y);
        }
    }

    /**
     * Graphics class for rendering Googly Eyes on a graphic overlay given the current eye positions.
     */
    class GooglyEyesGraphic extends GraphicOverlay.Graphic {

        private static final float EYE_RADIUS_PROPORTION = 0.45f;
        private static final float IRIS_RADIUS_PROPORTION = EYE_RADIUS_PROPORTION / 2.0f;

        private Paint mEyeWhitesPaint;
        private Paint mEyeIrisPaint;
        private Paint mEyeOutlinePaint;
        private Paint mEyeLidPaint;

        private EyePhysics mLeftPhysics = new EyePhysics();
        private EyePhysics mRightPhysics = new EyePhysics();

        private volatile PointF mLeftPosition;
        private volatile boolean mLeftOpen;

        private volatile PointF mRightPosition;
        private volatile boolean mRightOpen;
        private Object AttributeSet;
        Activity activity;

        //==============================================================================================
        // Methods
        //==============================================================================================

        GooglyEyesGraphic(GraphicOverlay overlay, Activity googlyEyesActivity1) {

            super(overlay);
            this.activity=googlyEyesActivity1;
            mEyeWhitesPaint = new Paint();
            mEyeWhitesPaint.setColor(Color.WHITE);
            mEyeWhitesPaint.setStyle(Paint.Style.FILL);

            mEyeLidPaint = new Paint();
            mEyeLidPaint.setColor(Color.YELLOW);
            mEyeLidPaint.setStyle(Paint.Style.FILL);

            mEyeIrisPaint = new Paint();
            mEyeIrisPaint.setColor(Color.BLACK);
            mEyeIrisPaint.setStyle(Paint.Style.FILL);

            mEyeOutlinePaint = new Paint();
            mEyeOutlinePaint.setColor(Color.GRAY);
            mEyeOutlinePaint.setStyle(Paint.Style.STROKE);
            mEyeOutlinePaint.setStrokeWidth(5);

        }

        void updateEyes(PointF leftPosition, boolean leftOpen, PointF rightPosition, boolean rightOpen) {
            mLeftPosition = leftPosition;
            mLeftOpen = leftOpen;
            mRightPosition = rightPosition;
            mRightOpen = rightOpen;
            postInvalidate();
        }

        @Override
        public void draw(Canvas canvas) {
            PointF detectLeftPosition = mLeftPosition;
            PointF detectRightPosition = mRightPosition;
            if ((detectLeftPosition == null) || (detectRightPosition == null)) {
                return;
            }

            PointF leftPosition = new PointF(translateX(detectLeftPosition.x), translateY(detectLeftPosition.y));
            PointF rightPosition = new PointF(translateX(detectRightPosition.x), translateY(detectRightPosition.y));

            // Use the inter-eye distance to set the size of the eyes.
            float distance = (float) Math.sqrt(Math.pow(rightPosition.x - leftPosition.x, 2) + Math.pow(rightPosition.y - leftPosition.y, 2));
            eyeRadius = EYE_RADIUS_PROPORTION * distance;
            float irisRadius = IRIS_RADIUS_PROPORTION * distance;

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//==================================================================== Eye Detection =========================================================================================================================================================

            if (CommonUtills.EyeType.equalsIgnoreCase("L")){
                PointF leftIrisPosition =mLeftPhysics.nextIrisPosition(leftPosition, eyeRadius, irisRadius);
                drawEye(canvas, leftPosition, eyeRadius, leftIrisPosition, irisRadius, mLeftOpen);
            }else {
                // Advance the current right iris position, and draw right eye.
                PointF rightIrisPosition = mRightPhysics.nextIrisPosition(rightPosition, eyeRadius, irisRadius);
                drawEye(canvas, rightPosition, eyeRadius, rightIrisPosition, irisRadius, mRightOpen);
            }

        }
        /**
         * Draws the eye, either closed or open with the iris in the current position.
         */
        private void drawEye(Canvas canvas, PointF eyePosition, float eyeRadius, PointF irisPosition, float irisRadius, boolean isOpen) {
        /*if (isOpen) {
            canvas.drawCircle(eyePosition.x, eyePosition.y, eyeRadius, mEyeWhitesPaint);
            canvas.drawCircle(irisPosition.x, irisPosition.y, irisRadius, mEyeIrisPaint);
        } else {
            canvas.drawCircle(eyePosition.x, eyePosition.y, eyeRadius, mEyeLidPaint);
            float y = eyePosition.y;
            float start = eyePosition.x - eyeRadius;
            float end = eyePosition.x + eyeRadius;
            canvas.drawLine(start, y, end, y, mEyeOutlinePaint);
        }*/
            canvas.drawCircle(eyePosition.x, eyePosition.y, eyeRadius, mEyeOutlinePaint);
            Log.d("rd", String.valueOf(eyeRadius));
            if (eyeRadius>200){

                Intent intent = new Intent(GooglyEyesActivity.this,GooglyEyesActivity.class);
                intent.putExtra("tp","tp");
                startActivity(intent);
                finish();
                /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O){
                    try {
                        camera = android.hardware.Camera.open();
                        Log.d("kl", String.valueOf(camera));
                        try {
                            Camera.Parameters parameters = camera.getParameters();
                            parameters.setFlashMode(getFlashOnParameter());
                            int maxZoom = parameters.getMaxZoom();
                            int zoom=10;
                            if (parameters.isZoomSupported()) {
                                if (zoom >=0 && zoom < maxZoom) {
                                    parameters.setZoom(zoom);
                                } else {
                                    //zoom parameter is incorrect
                                }
                            }
                            camera.setParameters(parameters);
                            camera.setPreviewTexture(new SurfaceTexture(0));
                            camera.startPreview();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        camera.takePicture(null,null,mPicture);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                } else{
                    closeCamera();
                }*/
            }
        }

        private String getFlashOnParameter() {
            List<String> flashModes = camera.getParameters().getSupportedFlashModes();
            if (flashModes.contains(FLASH_MODE_TORCH)) {
                return FLASH_MODE_TORCH;
            } else if (flashModes.contains(FLASH_MODE_ON)) {
                return FLASH_MODE_ON;
            } else if (flashModes.contains(FLASH_MODE_AUTO)) {
                return FLASH_MODE_AUTO;
            }
            throw new RuntimeException();
        }
    }

}
