package com.mylumineyes.Activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.mylumineyes.util.CommonUtills
import com.mylumineyes.EyeDetection.camera.GooglyEyesActivity
import com.mylumineyes.R


class LeftEye : AppCompatActivity() {

    internal var fragment: Fragment? = null
    lateinit var transaction: FragmentTransaction

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_left_eye)

        if (supportActionBar != null && Build.VERSION.SDK_INT >= 21) {
            supportActionBar!!.hide()
            window.setStatusBarColor(getResources().getColor(R.color.colorBlack))
        }

        val LeftEyeCamera: LinearLayout = findViewById(R.id.LeftEyeCamera)

        LeftEyeCamera.setOnClickListener {
            CommonUtills.EyeType="L"
            GooglyEyesActivity.left_bitmap = null
            var intent = Intent(this@LeftEye,GooglyEyesActivity::class.java)
            startActivity(intent)
        }
    }

    fun OpenFragment(Fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
        transaction.add(R.id.LeftEyeFragment, Fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun ClosedFragment() {
        if (fragment != null) {
            fragment = supportFragmentManager.findFragmentById(R.id.RightEyeFragment)
            transaction = supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(R.anim.exit_to_left, R.anim.exit_to_right)
            transaction.remove(fragment!!)
            transaction.commit()

        } else {
            //super.onBackPressed();
            fragment = supportFragmentManager.findFragmentById(R.id.RightEyeFragment)
            transaction = supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(R.anim.exit_to_left, R.anim.exit_to_right)
            transaction.remove(fragment!!)
            transaction.commit()
        }
    }

}