package com.mylumineyes.Activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import com.mylumineyes.EyeDetection.camera.GooglyEyesActivity
import com.mylumineyes.EyeDetection.camera.GooglyEyesActivity.RightEyePath
import com.mylumineyes.EyeDetection.camera.GooglyEyesActivity.bitmap
import com.mylumineyes.R
import kotlinx.android.synthetic.main.activity_capture_right_eye.*
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class CaptureRightEyeActivity : AppCompatActivity() {

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_capture_right_eye)

        if (supportActionBar != null && Build.VERSION.SDK_INT >= 21) {
            supportActionBar!!.hide()
            window.setStatusBarColor(getResources().getColor(R.color.colorBlack))
        }

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                GooglyEyesActivity.camera!!.stopPreview()
                GooglyEyesActivity.camera!!.release()
                GooglyEyesActivity.camera =null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        right_eye.setImageBitmap(bitmap)

        saveToInternalStorage(bitmap)

        Contact_Arrow.setOnClickListener{
            var LeftEye = Intent(this@CaptureRightEyeActivity, LeftEye::class.java)
            startActivity(LeftEye)
            finish()
        }

    }

    @SuppressLint("NewApi")
    private fun saveToInternalStorage(bitmapImage: Bitmap) {

        val root = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/saved_images")
        myDir.mkdirs()

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val fname = "right$timeStamp.jpg"

        val file = File(myDir, fname)
        if (file.exists()) file.delete()
        try {
            val out = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
            RightEyePath = file.absolutePath
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
