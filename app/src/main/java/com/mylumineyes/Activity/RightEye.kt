package com.mylumineyes.Activity

import android.os.Build
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.mylumineyes.Fragments.TipsFragment
import com.mylumineyes.R

class RightEye : AppCompatActivity() {

    internal var fragment: Fragment? = null
    lateinit var transaction: FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_right_eye)

        if (supportActionBar != null && Build.VERSION.SDK_INT >= 21) {
            supportActionBar!!.hide()
            window.setStatusBarColor(getResources().getColor(R.color.colorBlack))
        }

        val Tips: LinearLayout = findViewById(R.id.Tips)
        Tips.setOnClickListener { OpenFragment(TipsFragment()) }

    }

    fun OpenFragment(Fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
        transaction.replace(R.id.RightEyeFragment, Fragment, "Next")
        transaction.addToBackStack("tips")
        transaction.commit()
    }

    fun ClosedFragment() {
        if (fragment != null) {
            fragment = supportFragmentManager.findFragmentById(R.id.RightEyeFragment)
            transaction = supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(R.anim.exit_to_left, R.anim.exit_to_right)
            transaction.remove(fragment!!)
            transaction.commit()

        } else {
            //super.onBackPressed();
            fragment = supportFragmentManager.findFragmentById(R.id.RightEyeFragment)
            transaction = supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(R.anim.exit_to_left, R.anim.exit_to_right)
            transaction.remove(fragment!!)
            transaction.commit()
        }
    }

}
