package com.mylumineyes.Activity

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.mylumineyes.R

class SplashActivity : AppCompatActivity() {

    private val SPLASH_TIME_OUT: Long = 3000

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (supportActionBar != null && Build.VERSION.SDK_INT >= 21)
        {
            supportActionBar!!.hide()
            //window.setStatusBarColor(getResources().getColor(R.color.colorBlack))
        }

        Handler().postDelayed({
            startActivity(Intent(this, com.mylumineyes.Activity.UserGuideActivity::class.java))
            finish()
        },SPLASH_TIME_OUT)
    }
}
