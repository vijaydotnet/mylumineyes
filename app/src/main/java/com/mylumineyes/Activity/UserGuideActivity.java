package com.mylumineyes.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.mylumineyes.R;

public class UserGuideActivity extends AppCompatActivity {

    ViewPager viewPager;
    MyViewPagerAdapter myViewPagerAdapter;
    LinearLayout dotsLayout, Skiplayout;
    RelativeLayout NextLayout, StartLayout;
    TextView[] dots, Skip;
    int[] layouts;
    Button Button_Started;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_guide);

        prefManager = new PrefManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.layoutDots);


        Button_Started = findViewById(R.id.Button_Started);
        Button_Started.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserGuideActivity.this, RightEye.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);

            }
        });

        Skiplayout = findViewById(R.id.Skiplayout);
        Skiplayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    launchHomeScreen();
                }
        });

        //  Layouts of All Welcome Sliders
        layouts = new int[]{R.layout.screen_1, R.layout.screen_2, R.layout.screen_3};

        //  adding bottom dots
        addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

    }

    private void addBottomDots(int currentPage)
    {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
       // prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(UserGuideActivity.this, RightEye.class));
        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        finish();
    }



    //  Viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener()
    {
        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            //  changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1)
            {
                // last page. make button text to GOT IT
                Button_Started = findViewById(R.id.Button_Started);
                LinearLayout dotlayout = findViewById(R.id.layoutDots);
                Skiplayout = findViewById(R.id.Skiplayout);

                if (Button_Started.getVisibility() == View.INVISIBLE )
                {
                    Button_Started.setVisibility(View.VISIBLE);
                   dotlayout.setVisibility(View.INVISIBLE);
                   Skiplayout.setVisibility(View.INVISIBLE);
                }
            }
            else if (position < layouts.length - 1)
            {
                Button_Started = findViewById(R.id.Button_Started);
                LinearLayout dotlayout = findViewById(R.id.layoutDots);
                Skiplayout = findViewById(R.id.Skiplayout);

                if (Button_Started.getVisibility() == View.VISIBLE)
                {
                    Button_Started.setVisibility(View.INVISIBLE);
                    Skiplayout.setVisibility(View.VISIBLE);
                    dotlayout.setVisibility(View.VISIBLE);
                    }
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * Making notification bar transparent
     private void changeStatusBarColor() {
     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
     Window window = getWindow();
     window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
     window.setStatusBarColor(Color.TRANSPARENT);
     }
     }*/

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter
    {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter()
        {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount()
        {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
