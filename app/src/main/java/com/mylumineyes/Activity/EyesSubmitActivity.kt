package com.mylumineyes.Activity

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import cn.pedant.SweetAlert.SweetAlertDialog
import com.example.eyestesting.CustomSpinnerAdapter
import com.mylumineyes.APIService
import com.mylumineyes.EyeDetection.camera.GooglyEyesActivity
import com.mylumineyes.EyeDetection.camera.GooglyEyesActivity.LeftEyePath
import com.mylumineyes.EyeDetection.camera.GooglyEyesActivity.RightEyePath
import com.mylumineyes.Fragments.Successful
import com.mylumineyes.InfoPostModel
import com.mylumineyes.R
import com.mylumineyes.util.ApiUtils
import com.mylumineyes.util.CommonUtills
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_eyes_submit.*
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class EyesSubmitActivity : AppCompatActivity() {
    var progressDialog: ProgressDialog?=null
    var country = arrayOf("Country", "India", "USA", "China", "South Africa")
    var city = arrayOf("City", "Churu", "Jaipur", "Alwar", "Sikar")

    private var actualImage: File? = null
    private var compressedImage: File? = null

    private var leftactualImage: File? = null
    private var leftcompressedImage: File? = null

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eyes_submit)

        if (supportActionBar != null && Build.VERSION.SDK_INT >= 21) {
            supportActionBar!!.hide()
            window.setStatusBarColor(getResources().getColor(R.color.colorBlack))
        }

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                GooglyEyesActivity.camera!!.stopPreview()
                GooglyEyesActivity.camera!!.release()
                GooglyEyesActivity.camera =null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage("Please Wait..")
        progressDialog!!.setCancelable(true)

        val Countryspinner = CustomSpinnerAdapter(this, country)
        Country_Spinner!!.adapter = Countryspinner

        val Cityspinner = CustomSpinnerAdapter(this, city)
        City_Spinner!!.adapter = Cityspinner
        actualImage = File(GooglyEyesActivity.RightEyePath)
        leftactualImage = File(GooglyEyesActivity.LeftEyePath)

        compressImage()

        Button_Send.setOnClickListener {

            if (ed_name.getText().toString().contentEquals("")){
                Name_Layout.setError("Please Enter Your Name")
            } else if (ed_phone.getText().toString().contentEquals("")){
                Phone_Layout.setError("Please Enter Your Mobile No.")
            } else if (ed_phone.getText().toString().length<10){
                Phone_Layout.setError("Please Enter Your valid Mobile No.")
            } else if (ed_email.getText().toString().contentEquals("")){
                Email_Layout.setError("Please Enter Your Email")
            } else if(!isValidEmail(ed_email.getText().toString())){
                Email_Layout.setError("Please Enter your Valid email")
            } else if (ed_observation.getText().toString().contentEquals("")){
                Observation_Layout.setError("Please enter observation")
            } else {
                progressDialog!!.show()
                var file = File(GooglyEyesActivity.RightEyePath)
                var file2 = File(GooglyEyesActivity.LeftEyePath)
                val requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file)
                val body1 = MultipartBody.Part.createFormData("file1", file.name.replace(":", "_"), requestFile1)
                val requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2)
                val body2 = MultipartBody.Part.createFormData("file1",file2.name.replace(":", "_"), requestFile2)

                var mAPIService: APIService? = ApiUtils.apiService
                mAPIService!!.UploadImage(body1, body2)
                    .enqueue(object : retrofit2.Callback<ResponseBody> {
                        override fun onResponse(call: retrofit2.Call<ResponseBody>,response: retrofit2.Response<ResponseBody>) {
                            if (response.isSuccessful()) {
                                //progressDialog!!.dismiss()
                                //Toast.makeText(this@EyesSubmitActivity, "first", Toast.LENGTH_LONG).show()
                                val userType = response.body().string()
                                var jsonObject = JSONObject(userType)
                                var status = jsonObject.getBoolean("Status")
                                var message = jsonObject.getString("Message")
                                if (status == true && message.contentEquals("Images uploaded successfully.")) {
                                    var jsonObject1 = jsonObject.getJSONObject("Data")
                                    var file1 = jsonObject1.getString("File1")
                                    var file2 = jsonObject1.getString("File2")
                                    SendEyeData(file1, file2)
                                }
                            } else {
                                progressDialog!!.dismiss()
                                var error = response.errorBody().string()
                                Toast.makeText(this@EyesSubmitActivity,"" + error,Toast.LENGTH_LONG).show()
                            }
                        }override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                            progressDialog!!.dismiss()
                            Toast.makeText(this@EyesSubmitActivity,"" + t.message,Toast.LENGTH_LONG).show()
                            Log.i("", "error" + t.message)
                        }
                    })
            }
        }

    }

    private fun compressImage() {
            actualImage?.let { imageFile ->
                lifecycleScope.launch {
                    compressedImage = Compressor.compress(this@EyesSubmitActivity, imageFile)
                    setCompressedImage()
                }
            }

        leftactualImage?.let { imageFile ->
            lifecycleScope.launch {
                leftcompressedImage = Compressor.compress(this@EyesSubmitActivity, imageFile)
                saveleftCompressedImage()
            }
        }

    }

    @SuppressLint("NewApi")
    private fun saveleftCompressedImage() {
        leftcompressedImage?.let {
            //compressedImageView.setImageBitmap(BitmapFactory.decodeFile(it.absolutePath))
            //compressedSizeTextView.text = String.format("Size : %s", getReadableFileSize(it.length()))
            //Toast.makeText(this, "Compressed image save in " + it.path, Toast.LENGTH_LONG).show()
            //Log.d("Compressor", "Compressed image save in " + it.path)

            val root = Environment.getExternalStorageDirectory().toString()
            val myDir = File("$root/cmpsaved_images")
            myDir.mkdirs()

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val fname = "cmpleft$timeStamp.jpg"

            val file = File(myDir, fname)
            if (file.exists()) file.delete()
            try {
                val out = FileOutputStream(file)
                BitmapFactory.decodeFile(it.absolutePath).compress(Bitmap.CompressFormat.JPEG, 100, out)
                LeftEyePath = file.absolutePath
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    @SuppressLint("NewApi")
    private fun setCompressedImage() {
        compressedImage?.let {
            //compressedImageView.setImageBitmap(BitmapFactory.decodeFile(it.absolutePath))
            //compressedSizeTextView.text = String.format("Size : %s", getReadableFileSize(it.length()))
            //Toast.makeText(this, "Compressed image save in " + it.path, Toast.LENGTH_LONG).show()
            //Log.d("Compressor", "Compressed image save in " + it.path)

            val root = Environment.getExternalStorageDirectory().toString()
            val myDir = File("$root/cmpsaved_images")
            myDir.mkdirs()

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val fname = "cmpright$timeStamp.jpg"

            val file = File(myDir, fname)
            if (file.exists()) file.delete()
            try {
                val out = FileOutputStream(file)
                BitmapFactory.decodeFile(it.absolutePath).compress(Bitmap.CompressFormat.JPEG, 100, out)
                RightEyePath = file.absolutePath
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    private fun isValidEmail(eMail: String): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(eMail)
        return matcher.matches()
    }


    private fun SendEyeData(file1: String, file2: String) {

        if (ed_name.getText().toString().contentEquals("")){
            Name_Layout.setError("Please Enter Your Name")
        } else if (ed_phone.getText().toString().contentEquals("")){
            Phone_Layout.setError("Please Enter Your Mobile No.")
        } else if (ed_email.getText().toString().contentEquals("")){
            Email_Layout.setError("Please Enter Your Email")
        } else if (ed_observation.getText().toString().contentEquals("")){
            Observation_Layout.setError("Please enter observation")
        } else {

            var infoPostModel = InfoPostModel()
            infoPostModel.fullName = ed_name.getText().toString().trim()
            infoPostModel.email = ed_email.getText().toString().trim()
            infoPostModel.phoneNo = ed_phone.getText().toString().trim()
            infoPostModel.obeservations = ed_observation.getText().toString().trim()
            infoPostModel.phoneNo = ed_phone.getText().toString().trim()
            infoPostModel.file1 = file1
            infoPostModel.file2 = file2
            var mAPIService: APIService? = ApiUtils.apiService
            mAPIService!!.SendData(infoPostModel).enqueue(object : retrofit2.Callback<ResponseBody> {
                    override fun onResponse(call: retrofit2.Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                        if (response.isSuccessful()) {
                            progressDialog!!.dismiss()
                            val userType = response.body().string()
                            var jsonObject = JSONObject(userType)
                            var status = jsonObject.getBoolean("Status")
                            var message = jsonObject.getString("Message")
                            if (status == true && message.contentEquals("Data posted successfully.")) {
                                /*SweetAlertDialog(this@EyesSubmitActivity, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Success!!")
                                    .setContentText(message)
                                    .show()*/
                                OpenFragment()
                                /*var jsonObject1 = jsonObject.getJSONObject("Data")
                                var file1 = jsonObject1.getString("File1")
                                var file2 = jsonObject1.getString("File2")
                                SendEyeData(file1, file2)*/
                            }
                        } else {
                            progressDialog!!.dismiss()
                            var error = response.errorBody().string()
                            Toast.makeText(this@EyesSubmitActivity, "" + error, Toast.LENGTH_LONG).show()
                        }
                    }
                    override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                        progressDialog!!.dismiss()
                        Toast.makeText(this@EyesSubmitActivity, "" + t.message, Toast.LENGTH_LONG).show()
                        Log.i("", "error" + t.message)
                    }
                })
        }
    }

    fun OpenFragment(){
        val fragment: Fragment = Successful()
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.add(R.id.eye_frame, fragment)
        transaction.addToBackStack(null)
        transaction.commitAllowingStateLoss()
    }
}
