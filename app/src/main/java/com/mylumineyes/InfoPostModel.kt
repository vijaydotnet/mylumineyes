package com.mylumineyes

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class InfoPostModel(

	@field:SerializedName("Obeservations")
	var obeservations: String? = null,

	@field:SerializedName("Email")
	var email: String? = null,

	@field:SerializedName("PhoneNo")
	var phoneNo: String? = null,

	@field:SerializedName("FullName")
	var fullName: String? = null,

	@field:SerializedName("File2")
	var file2: String? = null,

	@field:SerializedName("File1")
	var file1: String? = null
)