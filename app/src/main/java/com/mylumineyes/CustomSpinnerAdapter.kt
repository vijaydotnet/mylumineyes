package com.example.eyestesting

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.mylumineyes.R


class CustomSpinnerAdapter (internal var context: Context, internal var profile: Array<String>) : BaseAdapter() {

    internal var inflter: LayoutInflater

    init {
        inflter = LayoutInflater.from(context)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view = inflter.inflate(R.layout.custom_spinnerlist,null)
        val names = view.findViewById<View>(R.id.spinnerText) as TextView?

        names!!.text = profile[position]
        return view
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount() = profile.size
}