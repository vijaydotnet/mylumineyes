package com.mylumineyes.Fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.mylumineyes.Activity.RightEye
import com.mylumineyes.util.CommonUtills
import com.mylumineyes.EyeDetection.camera.GooglyEyesActivity
import com.mylumineyes.R

class TipsFragment : Fragment() {

    val REQUEST_IMAGE_CAPTURE = 1
    private val PERMISSION_REQUEST_CODE: Int = 101

    @SuppressLint("NewApi")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_tips, container, false)

        val Tips_arrow: ImageView = view.findViewById(R.id.Tips_arrow)
        Tips_arrow.setOnClickListener { (activity as RightEye).ClosedFragment() }
        val Button_agree: Button = view.findViewById(R.id.Button_agree)

        Button_agree.setOnClickListener {
             //val camera = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
             //startActivity(camera)
            CommonUtills.EyeType = "R"
            GooglyEyesActivity.bitmap=null
            val intent = Intent(activity, GooglyEyesActivity::class.java)
            startActivity(intent)
            //(activity as RightEye).OpenFragment(Capture_RightEye())
        }

        return view

    }

}