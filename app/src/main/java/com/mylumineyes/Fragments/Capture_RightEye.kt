package com.mylumineyes.Fragments

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.mylumineyes.Activity.LeftEye
import com.mylumineyes.EyeDetection.camera.GooglyEyesActivity

import com.mylumineyes.R
import kotlinx.android.synthetic.main.fragment_capture__right_eye.*

/**
 * A simple [Fragment] subclass.
 */
class Capture_RightEye : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_capture__right_eye, container, false)

       var rightEye  = view.findViewById<ImageView>(R.id.right_eye)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rightEye.setImageBitmap(GooglyEyesActivity.bitmap)
        }

        val Contact_Arrow:LinearLayout = view.findViewById(R.id.Contact_Arrow)
        Contact_Arrow.setOnClickListener{
            var LeftEye = Intent(context, LeftEye::class.java)
            startActivity(LeftEye)
        }
        return view
    }

}
