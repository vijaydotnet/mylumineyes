package com.mylumineyes.Fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.mylumineyes.Activity.EyesSubmitActivity
import com.mylumineyes.EyeDetection.camera.GooglyEyesActivity.camera
import com.mylumineyes.EyeDetection.camera.GooglyEyesActivity
import com.mylumineyes.R

/**
 * A simple [Fragment] subclass.
 */
class EyesImages : Fragment() {
    var rightImage: ImageView?=null
    var leftImage: ImageView?=null
    @SuppressLint("NewApi")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_eyes_images, container, false)

            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    camera!!.stopPreview()
                    camera!!.release()
                    camera=null
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        val Contact_Arrow: LinearLayout = view.findViewById(R.id.Contact_Arrow)

        rightImage = view.findViewById(R.id.right)
        leftImage = view.findViewById(R.id.left)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rightImage!!.setImageBitmap(GooglyEyesActivity.bitmap)
            leftImage!!.setImageBitmap(GooglyEyesActivity.left_bitmap)
        }

        /*val imgFile = File(CommonUtills.RightEyePath)
        if (imgFile.exists()) {
            val myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath())
            rightImage!!.setImageBitmap(myBitmap)
        }*/

        /*val imgFile1 = File(CommonUtills.LeftEyePath)
        if (imgFile1.exists()) {
            val myBitmap = BitmapFactory.decodeFile(imgFile1.getAbsolutePath())
            leftImage!!.setImageBitmap(myBitmap)
        }*/

        Contact_Arrow.setOnClickListener{
            /*val fragment = EyesInformationActivity()
            val manager = activity!!.getSupportFragmentManager()
            val transaction = manager.beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
            transaction.replace(R.id.eye_frame, fragment)
            transaction.addToBackStack(null)
            transaction.commit()*/

            var intent = Intent(activity,EyesSubmitActivity::class.java)
            startActivity(intent)

        }

        return view
    }

    /*private fun getFlashOnParameter(): String {
        val flashModes = camera!!.getParameters().supportedFlashModes

        if (flashModes.contains(FLASH_MODE_TORCH)) {
            return FLASH_MODE_TORCH
        } else if (flashModes.contains(FLASH_MODE_OFF)) {
            return FLASH_MODE_OFF
        } else if (flashModes.contains(FLASH_MODE_AUTO)) {
            return FLASH_MODE_AUTO
        }
        throw RuntimeException()
    }*/

}
