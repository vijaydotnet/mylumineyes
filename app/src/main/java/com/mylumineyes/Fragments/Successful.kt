package com.mylumineyes.Fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.mylumineyes.Activity.RightEye

import com.mylumineyes.R

class Successful : Fragment() {

    var btnok: Button?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_successful, container, false)

        btnok = view.findViewById(R.id.ok)

        btnok!!.setOnClickListener {
            var  intent = Intent(activity,RightEye::class.java)
            startActivity(intent)
            activity!!.finish()
        }
        return view
    }

}
