package com.mylumineyes.util

import com.mylumineyes.APIService
import com.mylumineyes.RestClient

object ApiUtils {
    val BASE_URL = "http://adamadam1-001-site1.btempurl.com/"
    val apiService: APIService get() = RestClient.getClient(BASE_URL)!!.create(APIService::class.java)

    val apiService1: APIService get() = RestClient.getRetofitAuth(BASE_URL)!!.create(APIService::class.java)
}