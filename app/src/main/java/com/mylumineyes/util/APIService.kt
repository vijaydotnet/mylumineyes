package com.mylumineyes


import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface APIService {

    @POST("api/uploadImages")
    @Multipart
    fun UploadImage(@Part file1: MultipartBody.Part,
                    @Part file2: MultipartBody.Part): Call<ResponseBody>

    @POST("api/saveData")
    fun SendData(@Body infoPostModel: InfoPostModel): Call<ResponseBody>

}
