package com.mylumineyes

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object RestClient {

    var retrofit: Retrofit? = null

    fun getClient(baseUrl: String): Retrofit? {
        if (retrofit == null) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .build()

            retrofit = Retrofit.Builder()
                    .client(client)
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }

        return retrofit

    }

    fun getRetofitAuth(baseUrl: String): Retrofit? {

            val okHttpClient = OkHttpClient().newBuilder().addInterceptor { chain ->
                val originalRequest = chain.request()
                val builder = originalRequest.newBuilder().header("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjkwY2MyOGZiLTk2YTEtNGNlZC04Y2MwLTIwY2VlNDljMGNiYSIsIm5iZiI6MTU4MTY1OTA3NSwiZXhwIjoxNTgxNzQ1NDc1LCJpYXQiOjE1ODE2NTkwNzV9.IpQDhEsHXlm5DiVC-oCWOqb-VcVK13Zn5OLLTV2UYaw")
                val newRequest = builder.build()
                chain.proceed(newRequest)
            }.build()

            retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(okHttpClient)
                    .build()


        return retrofit
    }

}

