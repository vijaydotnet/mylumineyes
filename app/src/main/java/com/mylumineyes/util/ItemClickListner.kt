package com.mylumineyes

import android.view.View

interface ItemClickListner {
   fun onClick(view: View, position: Int)
}